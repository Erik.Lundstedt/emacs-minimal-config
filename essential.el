
(defvar efs/default-font-size 180)
(defvar efs/default-variable-font-size 180)

;; Make frame transparency overridable
;; (defvar efs/frame-transparency '(100 . 100))

(defvar efs/frame-transparency '(90 . 90))



(setq-default
	ad-redefinition-action 'accept		; Silence warnings for redefinition
	auto-save-list-file-prefix nil		; Prevent tracking for auto-saves
	cursor-in-non-selected-windows nil	; Hide the cursor in inactive windows
	cursor-type '(hbar . 2)				; Underline-shaped cursor
	custom-unlispify-menu-entries nil		; Prefer kebab-case for titles
	custom-unlispify-tag-names nil		; Prefer kebab-case for symbols
	delete-by-moving-to-trash t			; Delete files to trash
	fill-column 180						; Set width for automatic line breaks
	gc-cons-threshold (* 8 1024 1024)		; We're not using Game Boys anymore
	help-window-select t					; Focus new help windows when opened
	;;inhibit-startup-screen t			; Disable start-up screen
	initial-scratch-message ""			; Empty the initial *scratch* buffer
	mouse-yank-at-point t					; Yank at point rather than pointer
	read-process-output-max (* 1024 1024)	; Increase read size per process
	recenter-positions '(5 top bottom)	; Set re-centering positions
	scroll-conservatively 101				; Avoid recentering when scrolling far
	scroll-margin 2						; Add a margin when scrolling vertically
	select-enable-clipboard t				; Merge system's and Emacs' clipboard
	sentence-end-double-space nil			; Use a single space after dots
	show-help-function nil				; Disable help text everywhere
	tab-always-indent 'nil				; Indent first then try completions
	tab-width 4							; Smaller width for tab characters
	indent-tabs-mode t					; Start using tabs to indent
	uniquify-buffer-name-style 'forward	; Uniquify buffer names
	warning-minimum-level :error			; Skip warning buffers
	window-combination-resize t			; Resize windows proportionally
	x-stretch-cursor t)					; Stretch cursor to the glyph width
(blink-cursor-mode 0)					; Prefer a still cursor
(delete-selection-mode 1)				; Replace region when inserting text
(fset 'yes-or-no-p 'y-or-n-p)			; Replace yes/no prompts with y/n
(global-subword-mode 1)				; Iterate through CamelCase words
(mouse-avoidance-mode 'exile)			; Avoid collision of mouse with point
(put 'downcase-region 'disabled nil)	; Enable downcase-region
(put 'upcase-region 'disabled nil)		; Enable upcase-region
(set-default-coding-systems 'utf-8)	; Default to utf-8 encoding

















;; Initialize package sources
(require 'package)

(setq package-archives '(
		 ("melpa" . "https://melpa.org/packages/")
		 ("org" . "https://orgmode.org/elpa/")
		 ("elpa" . "https://elpa.gnu.org/packages/")
		 ("melpa" . "https://raw.githubusercontent.com/d12frosted/elpa-mirror/master/melpa/")
		 ("org"   . "https://raw.githubusercontent.com/d12frosted/elpa-mirror/master/org/")
		 ("gnu"   . "https://raw.githubusercontent.com/d12frosted/elpa-mirror/master/gnu/")
		 ("melpa" . "https://gitlab.com/d12frosted/elpa-mirror/raw/master/melpa/")
		 ("org"   . "https://gitlab.com/d12frosted/elpa-mirror/raw/master/org/")
		 ("gnu"   . "https://gitlab.com/d12frosted/elpa-mirror/raw/master/gnu/")

))





(package-initialize)

(unless package-archive-contents
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)



(use-package evil)
(evil-mode 1)



(scroll-bar-mode t)        ; Disable visible scrollbar
(tool-bar-mode -1)          ; Disable the toolbar
(tooltip-mode -1)           ; Disable tooltips
(set-fringe-mode 2)        ; Give some breathing room
(menu-bar-mode t)            ;(dont) Disable the menu bar

;; Set up the visible bell
(setq visible-bell nil)
(column-number-mode)
;; Set frame transparency
(set-frame-parameter (selected-frame) 'alpha efs/frame-transparency)
(add-to-list 'default-frame-alist `(alpha . ,efs/frame-transparency))
(setq truncate-lines nil)
(setq frame-resize-pixelwise t)


(use-package doom-themes
  :init (load-theme 'doom-palenight t))

(use-package all-the-icons)

(use-package doom-modeline
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15))
  :config
  (setq doom-modeline-icon t)
  )

(defun el/setLinenumbers ()
  "This sets up my prefered linenumber settings."
  (interactive)
  (global-display-line-numbers-mode t)
  (setq display-line-numbers 'relative)
  (menu-bar-display-line-numbers-mode 'relative)
  )
(add-hook 'prog-mode-hook 'el/setLinenumbers)
(add-hook 'org-mode-hook 'el/setLinenumbers)

(el/setLinenumbers)

(defun el/set-font-faces ()
  (message "Setting faces!")

  (set-face-attribute 'default nil :font "UbuntuMono NF"
					  :height efs/default-font-size)

  ;; Set the fixed pitch face
  (set-face-attribute 'fixed-pitch nil :font "UbuntuMono NF"
					  :height efs/default-font-size)

  ;; Set the variable pitch face
  (set-face-attribute 'variable-pitch nil :font "UbuntuMono NF"
					  :height efs/default-variable-font-size :weight 'regular)
  )
