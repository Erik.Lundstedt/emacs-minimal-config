	  (if (daemonp)
		  (add-hook 'after-make-frame-functions
				 (lambda (frame)
			  ;; (setq doom-modeline-icon t)
			  (with-selected-frame frame
				 (el/set-font-faces))))
		 (el/set-font-faces))

	  (use-package speedbar
		 :ensure nil
		 :config
		 (setq speedbar-update-flag t)
		 (setq speedbar-track-mouse-flag t)
		 (setq speedbar-use-images nil)
		 (setq speedbar-default-position `left)
		 (define-key speedbar-mode-map (kbd "<tab>") 'speedbar-toggle-line-expansion)
		 )
	  (speedbar-frame-mode t)

	  (speedbar-add-supported-extension "sh")
	  (speedbar-add-supported-extension "lua")
	  (speedbar-add-supported-extension "org")

	  (setq speedbar-update-flag t)
	  (setq speedbar-track-mouse-flag t)
	  (setq speedbar-use-images nil)
	  (setq speedbar-default-position `left)
	  (define-key speedbar-mode-map (kbd "<tab>") 'speedbar-toggle-line-expansion)





;; Enable vertico
(use-package vertico
  :init
  (vertico-mode)

  ;; Different scroll margin
  ;; (setq vertico-scroll-margin 0)

  ;; Show more candidates
   (setq vertico-count 16)

  ;; Grow and shrink the Vertico minibuffer
  (setq vertico-resize 'fixed)
  ;; Optionally enable cycling for `vertico-next' and `vertico-previous'.
   (setq vertico-cycle t)
  )
(use-package try)


;; Configure directory extension.
;; (use-package vertico-buffer
;;   :after vertico
;;   :ensure nil
;; )



;; Optionally use the `orderless' completion style. See
;; `+orderless-dispatch' in the Consult wiki for an advanced Orderless style
;; dispatcher. Additionally enable `partial-completion' for file path
;; expansion. `partial-completion' is important for wildcard support.
;; Multiple files can be opened at once with `find-file' if you enter a
;; wildcard. You may also give the `initials' completion style a try.
(use-package orderless
  :init
  ;; Configure a custom style dispatcher (see the Consult wiki)
  ;; (setq orderless-style-dispatchers '(+orderless-dispatch)
  ;;       orderless-component-separator #'orderless-escapable-split-on-space)
  (setq completion-styles '(orderless)
	completion-category-defaults nil
	completion-category-overrides '((file (styles partial-completion)))))

;; Persist history over Emacs restarts. Vertico sorts by history position.
;; (use-package savehist
;;   :init
;;   (savehist-mode))

;; A few more useful configurations...
(use-package emacs
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; Alternatively try `consult-completing-read-multiple'.
  (defun crm-indicator (args)
    (cons (concat "[CRM] " (car args)) (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
	'(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t))









