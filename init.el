


;; Initialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))



(global-set-key (kbd "<escape>") 'keyboard-escape-quit)



(package-install 'evil)

   (setq evil-want-integration t)
   (setq evil-want-keybinding nil)
   (setq evil-want-C-u-scroll t)
   (setq evil-want-C-i-jump nil)
   (evil-mode 1)
   (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
   (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

   ;; Use visual line motions even outside of visual-line-mode buffers
   (evil-global-set-key 'motion "j" 'evil-next-visual-line)
   (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

   (evil-set-initial-state 'messages-buffer-mode 'normal)
   (evil-set-initial-state 'dashboard-mode 'normal)

 (package-install 'evil-collection)
   (evil-collection-init)


;; (use-package doom-themes
;;   :init (load-theme 'doom-palenight t))


(defvar el/font-size 180)

;; Make frame transparency overridable
;; (defvar e/frame-transparency '(100 . 100))

(defvar el/frame-transparency '(90 . 90))
(scroll-bar-mode t)        ; Disable visible scrollbar
(tool-bar-mode -1)          ; Disable the toolbar
(tooltip-mode -1)           ; Disable tooltips
(set-fringe-mode 2)        ; Give some breathing room
(menu-bar-mode t)            ;(dont) Disable the menu bar

;; Set up the visible bell
(setq visible-bell nil)
;; Set frame transparency
(set-frame-parameter (selected-frame) 'alpha el/frame-transparency)
(add-to-list 'default-frame-alist `(alpha . ,el/frame-transparency))
(setq truncate-lines nil)
(setq frame-resize-pixelwise t)

(icomplete-mode -1)
(icomplete-vertical-mode)
(show-paren-mode)

(require 'org)

(require 'speedbar)
(setq speedbar-update-flag t)
(setq speedbar-track-mouse-flag t)
(setq speedbar-use-images nil)
(setq speedbar-default-position `left)
(speedbar)







(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages '(evil)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
